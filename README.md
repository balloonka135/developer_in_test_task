## API for movies app.
#### You need to create API with 3 endpoints:
- Get person info by person_id /people/:person_id
- Get person movies filtered by genres /people/:person_id/movies?genres=genre1,genre2
- Get movie info by imdb_id movies /movies/:movie_id

#### Examples:
- /people/nm0000001
    ```json
    {
        "id": "nm0000001",
        "name": "Fred Astaire",
        "yob": 1899,
        "alive": false,
        "age": 88
    }
    ```

- /people/nm0003757/movies?genres=Comedy,Family
    ```json
    {
        "id": "nm0003757",
        "name": "Jacqueline Lewis",
        "movies": [
            {"id": "tt0052676",
             "primary_title": "Carry on Teacher",
             "year": 1959},
            {"id": "tt0056751",
             "primary_title": "Doctor Who",
             "year_start": 1963,
             "year_end": 1989},
        ]
    }
    ```

- /movies/tt9163354
    ```json
    {
        "id": "tt9163354",
        "primary_title": "My Mother Told Me",
        "year": 2018,
        "duration": 2,
        "genres": ["Animation", "Biography", "Short"],
        "people": [
            {
                "id": "nm10206014",
                "name": "Yasmin Rai"},
            {
                "id": "nm10206015",
                "name": "Jalal Afzal"}]
    }
    ```

#### Data source
Use IMDB **title.basics**(movie) and **name.basics**(person) datasets from https://datasets.imdbws.com/.

#### Implementation details
Feel free to use any needed python frameworks and DB. Add any unit / integrational tests to ensure implementation matches business logic of described endpoints above
Project should contain:
- **db export script** for loading datasets to DB with certain structure
- **run file** for running API
- Submit your solution with merge request into this repo